import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, from, of } from 'rxjs';
import { AngularFireAuth } from '@angular/fire/auth';
import { tap, switchMap, map, finalize } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private isLoggedInSubject = new BehaviorSubject<boolean>(null);
  public isLoggedIn$ = this.isLoggedInSubject.asObservable();

  private idTokenSubject = new BehaviorSubject<string>(null);
  private userSubject = new BehaviorSubject<firebase.User>(null);

  constructor(
    private firebaseAuth: AngularFireAuth
  ) {
    this.firebaseAuth.authState
      .pipe(
        switchMap(user => {
          if (user) {
            return from(user.getIdToken())
              .pipe(
                tap(idToken => this.idTokenSubject.next(idToken)),
                map(() => user)
              );
          } else {
            this.idTokenSubject.next(null);
            return of(user);
          }
        })
      )
      .pipe(
        tap(user => this.userSubject.next(user))
      )
      .subscribe((user: firebase.User) => {
        // tslint:disable-next-line: no-string-literal
        window['getToken'] = () => this.idTokenSubject.value;
        this.isLoggedInSubject.next(!!user);
      });
  }

  public isLoggedIn(): boolean {
    return this.isLoggedInSubject.value;
  }

  public login(email: string, password: string): Observable<firebase.auth.UserCredential> {
    return from(this.firebaseAuth.signInWithEmailAndPassword(email, password));
  }

  public logout(): Observable<void> {
    return from(this.firebaseAuth.signOut())
      .pipe(finalize(() => this.isLoggedInSubject.next(false)));
  }

  public getUser(): Observable<firebase.User> {
    return from(this.firebaseAuth.user);
  }

  public getUserSync(): firebase.User {
    return this.userSubject.value;
  }

  public getIdToken(): string {
    return this.idTokenSubject.value;
  }
}
