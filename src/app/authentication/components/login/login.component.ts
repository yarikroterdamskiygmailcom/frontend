import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import {
  FormGroup,
  FormBuilder,
  AbstractControl,
  Validators,
} from '@angular/forms';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';
import { validateAllFormFields } from 'src/app/core/helpers/form.helpers';
import { NotificationService } from 'src/app/core/services/notification.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LoginComponent implements OnInit {
  public form: FormGroup;
  public showUserNotFoundError = false;

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private router: Router,
    private notificationService: NotificationService,
  ) { }

  public get email(): AbstractControl {
    return this.form.get('email');
  }

  public get password(): AbstractControl {
    return this.form.get('password');
  }

  ngOnInit(): void {
    this.form = this.formBuilder.group({
      email: ['', [Validators.email, Validators.required]],
      password: ['', [Validators.min(5), Validators.required]],
    });
  }

  public login(): void {
    if (!this.form.valid) {
      validateAllFormFields(this.form);
      return;
    }

    const { email, password } = this.form.getRawValue();
    this.authService.login(email, password)
      .subscribe(
        () => this.router.navigate(['/home']),
        (err: { message: string }) => this.notificationService.notify(err.message, 'Error')
      );
  }
}
