import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { IResource, ResourceKey } from '../interfaces/responses/IResource';
import { IDataResponseBody } from '../interfaces/responses/IDataResponse';
import { IResourceCollection } from '../interfaces/responses/IResourceCollectin';
import { AuthService } from '../../authentication/services/auth.service';
import { NotificationService } from './notification.service';
import { ResourceTrackerService } from './resource-tracker.service';
declare type Items<T extends IResource<ResourceKey>> = HttpResponse<IDataResponseBody<IResourceCollection<T>>>;

@Injectable({
  providedIn: 'root'
})
export class RepositoryService {
  constructor(
    public httpClient: HttpClient,
    public notificationsService: NotificationService,
    public resourceTracker: ResourceTrackerService,
    public authService: AuthService
  ) {
  }


  public mapToItemsOperator = <T extends IResource<ResourceKey>>() => {
    return map((response: Items<T>) => response.body.data.items);
  }
}
