import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { EHttpRequest } from '../classes/http/EHttpRequest';
import { LoadingIndicatorService } from './loading-indicator.service';
/**
 * This service is used to store the current outgoing http requests
 * we can add and delete requests
 */
@Injectable({
  providedIn: 'root'
})
export class RequestsService {
  private requests = new BehaviorSubject<EHttpRequest<any>[]>([]);
  constructor(
    private loaderService: LoadingIndicatorService,
  ) { }

  public removeRequest(request: EHttpRequest<any>) {
    const index = this.requests.value.indexOf(request);
    this.requests.value.splice(index, 1);
    this.requests.next(this.requests.value);
    this.loaderService.isLoadingSubject.next(this.requests.value.length > 0);
  }

  public removeRequests() {
    this.requests.next([]);
    this.loaderService.isLoadingSubject.next(false);
  }

  public addRequest(req: EHttpRequest<any>) {
    this.requests.next([...this.requests.value, req]);
  }
}
