import { Injectable } from '@angular/core';
import { ICacheItem } from '../interfaces/cache/ICacheItem';
import { AppException } from '../classes/exceptions/AppException';
import { InvalidArgumentException } from '../classes/exceptions/InvalidArgumentException';

// tslint:disable-next-line:class-name
interface _ICacheItem {
  value?: any;
  key: string;
  expiresAt?: Date;
}

interface Cache {
  items: Map<string, _ICacheItem>;
  deferredItems: Map<string, ICacheItem<any>>;
}

@Injectable({
  providedIn: 'root'
})
export class CacheService {

  private static validKeySymbols = /.+/;

  private cache: Cache = {
    items: new Map(),
    deferredItems: new Map(),
  };

  public getItem<T>(key: string): ICacheItem<T> {
    this.validateKey(key);
    const { items } = this.cache;
    const item = items.get(key);
    const isFound = items.has(key);
    const hasExpired = (
      !isFound ||
      (
        item.expiresAt === undefined ||
        item.expiresAt.getTime() < Date.now()
      )
    );

    if (hasExpired) {
      this.deleteItem(key);
    }

    const source: _ICacheItem = hasExpired ? { key } : item;

    return {
      key,
      value: source.value,
      expiresAt: source.expiresAt,
      isHit: !hasExpired
    };
  }

  public getItems<T>(keys: string[]): ICacheItem<T>[] {
    return keys.map(el => this.getItem<T>(el));
  }

  public getItemsByRegex<T>(regexp: RegExp): ICacheItem<T>[] {
    return this.getItemsBySelector((key) => regexp.test(key));
  }

  public getItemsByPrefix<T>(keyPrefix: string): ICacheItem<T>[] {
    return this.getItemsBySelector((key) => key.startsWith(keyPrefix));
  }

  public getItemsBySelector<T>(selector: (key: string) => boolean): ICacheItem<T>[] {
    const matchingItems: ICacheItem<T>[] = [];
    for (const key of this.cache.items.keys()) {
      if (!selector(key)) {
        continue;
      }
      const item = this.getItem<T>(key);
      if (!item.isHit) {
        continue;
      }
      matchingItems.push(item);

    }
    return matchingItems;
  }

  public hasItem(key: string): boolean {
    return this.getItem(key).isHit;
  }

  public clear(): boolean {
    this.cache.items.clear();

    return true;
  }

  public deleteItem(key: string): boolean {
    this.validateKey(key);

    return this.cache.items.delete(key);
  }

  public deleteItems(keys: string[]): boolean {
    keys.forEach(key => this.validateKey(key));
    keys.forEach(key => this.cache.items.delete(key));

    return true;
  }

  public save(item: ICacheItem<any>): boolean {
    this.cache.items.set(item.key, item);

    return true;
  }

  public saveDeferred(item: ICacheItem<any>): boolean {
    this.cache.deferredItems.set(item.key, item);
    return true;
  }

  public commit() {
    const { deferredItems } = this.cache;
    const prevCacheState = [];
    try {
      deferredItems.forEach(item => {
        prevCacheState.push(this.getItem(item.key));
        const isSuccess = this.save(item);
        if (!isSuccess) {
          throw new AppException(`Could not save item with key: ${item.key}`);
        }
      });
    } catch (e) {
      try {
        prevCacheState.forEach(item => {
          const isSuccess = item.isHit
            ? this.save(item)
            : this.deleteItem(item.key);
          if (!isSuccess) {
            throw new AppException('Rollback failed for cache commit');
          }
        });
      } catch (e) {
        console.warn(e);
      }
      return false;
    }

    return true;
  }

  private validateKey(key: string) {
    if (!CacheService.validKeySymbols.test(key)) {
      throw new InvalidArgumentException('Unsupported characters in key');
    }
  }
}
