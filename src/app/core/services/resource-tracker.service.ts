import { Injectable } from '@angular/core';
import { CacheService } from './cache.service';
import { ResourceKey, IResource } from '../interfaces/responses/IResource';
import { ICacheItem } from '../interfaces/cache/ICacheItem';

export const TTL_SHORT = 1000 * 60 * 1; // 1 min
export const TTL_MEDIUM = 1000 * 60 * 10; // 10 min
export const TTL_LONG = 1000 * 60 * 60; // 1h
export const TTL_FOREVER = 1000 * 60 * 60 * 24; // 1day

@Injectable({
  providedIn: 'root'
})
export class ResourceTrackerService {

  private namespace = '/resources';
  private resourcePath = `${this.namespace}/{type}/single/{id}`;
  private collectionPath = `${this.namespace}/{type}/collections/{url}`;

  constructor(
    private cacheService: CacheService,
  ) { }

  private getResourceKey<T extends ResourceKey>(resource: IResource<T>, type: string): string {
    return this.resourcePath
      .replace('{type}', type)
      .replace('{id}', resource.id.toString());
  }

  private getResourceCollectionKey(resourceType: string, url: string = ''): string {
    return this.collectionPath
      .replace('{type}', resourceType)
      .replace('{url}', url.replace(/^\//, ''));
  }

  private getResourceCollectionKeyPrefix(resourceType: string): string {
    return this.getResourceCollectionKey(resourceType, '');
  }

  public saveOne<Entity extends IResource<ResourceKey>>(
    resource: Entity,
    type: string,
    expiresAt?: Date,
  ): boolean {
    const item = this.cacheService.getItem(this.getResourceKey(resource, type));
    item.value = resource;
    item.expiresAt = expiresAt;

    return this.cacheService.save(item);
  }

  public getOne<Entity>(resource: IResource<ResourceKey>, type: string): ICacheItem<Entity> {
    return this.cacheService.getItem<Entity>(this.getResourceKey(resource, type));
  }

  public getCollection<Entity extends IResource<ResourceKey>>(
    resourceType: string,
    url: string
  ): ICacheItem<Entity[]> {
    return this.cacheService.getItem<Entity[]>(
      this.getResourceCollectionKey(resourceType, url)
    );
  }

  public saveCollection<Entity extends IResource<ResourceKey>>(
    resourceType: string,
    resources: Entity[],
    url: string,
    expiresAt: Date,
    type: string,
  ): boolean {
    resources.forEach(r => this.saveOne(r, type, expiresAt));
    const collectionItem = this.cacheService.getItem<Entity[]>(
      this.getResourceCollectionKey(resourceType, url)
    );
    collectionItem.value = resources;
    collectionItem.expiresAt = expiresAt;

    return this.cacheService.save(collectionItem);
  }

  public deleteCollection(baseUrl: string): boolean {
    return this.cacheService.deleteItem(baseUrl);
  }

  public deleteCollectionsByType(type: string): void {
    this.cacheService.deleteItems(
      this.cacheService
        .getItemsByPrefix(this.getResourceCollectionKeyPrefix(type))
        .map(item => item.key)
    );
  }

  public deleteOne<T extends ResourceKey>(resource: IResource<T>, type: string): boolean {
    return this.cacheService.deleteItem(this.getResourceKey(resource, type));
  }
}
