import * as blogsJSON from './blogs.mock.json';
import { IPost } from '../../interfaces/resources/IPost';


export const BLOGS_MOCK: IPost[] = (blogsJSON as any).default;
