export interface ITooltipInfo {
  age: number;
  displayName: string;
  gender: string;
  picture: string;
  userInfo: string;
  state: string;
  zip: string;
}
