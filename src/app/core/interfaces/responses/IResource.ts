export type ResourceKey = string;

export interface IResource<T> {
  id: T;
}
