export interface IResponseBody { }

export interface IDataResponseBody<T> extends IResponseBody {
  data: T;
}

export interface IErrorResponseBody<T> extends IResponseBody {
  error: T;
}
