import { IResource, ResourceKey } from './IResource';

export interface IResourceCollection<T extends IResource<ResourceKey>> {
  items: T[];
  _type: string;
  _itemType: string;
  count: number;
}
