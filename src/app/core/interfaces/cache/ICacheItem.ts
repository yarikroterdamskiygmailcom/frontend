export interface ICacheTagMeta {
  name: string;
  creationDate: Date;
}

export interface ICacheItem<T> {
  value?: T;
  key: string;
  isHit: boolean;
  expiresAt?: Date;
}
