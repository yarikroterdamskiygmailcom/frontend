export interface ISidenavItem {
  path: string;
  label: string;
  icon?: string;
  children?: ISidenavItem[];
}
