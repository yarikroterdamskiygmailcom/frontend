import { IResource, ResourceKey } from '../responses/IResource';

interface ISharedProps {
  name: string;
}

export interface ICategoryCreate extends ISharedProps { }

export interface ICategoryBase extends ISharedProps, IResource<ResourceKey> { }

export interface ICategoryDTO extends ICategoryBase { }

export interface ICategory extends ICategoryDTO, IResource<ResourceKey> { }
