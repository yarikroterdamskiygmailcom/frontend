import { IResource, ResourceKey } from '../responses/IResource';
import { IOption } from '../IOption';
export enum GENDER {
  male,
  female,
  other
}

export enum ROLE {
  admin,
  provider, // expert/doctor
  patient,
  friend
}

interface ISharedProps {
  firstName: string;
  lastName: string;
  displayName: string;
  categoryId: string;
  dateOfBirth: string; // iso string
  gender: GENDER;
  picture: string;
  role: ROLE;
  tel: string;
  about: string;
  // address
  countryId: string;
  stateId: string;
  city: string;
  zip: string;
}

export interface IUserCreate extends ISharedProps { }

export interface IUserBase extends ISharedProps, IResource<ResourceKey> { }

export interface IUserDTO extends IUserBase { }

export interface IUser extends IUserDTO, IResource<ResourceKey> { }


export const genderItems: IOption<GENDER>[] = [
  {
    label: 'Male',
    value: GENDER.male,
  },
  {
    label: 'Female',
    value: GENDER.female,
  },
  {
    label: 'Other',
    value: GENDER.other,
  },
];

export const rolesItems: IOption<ROLE>[] = [
  {
    label: 'Patient',
    value: ROLE.patient,
  },
  {
    label: 'Friend',
    value: ROLE.friend,
  },
  {
    label: 'Provider',
    value: ROLE.provider,
  },
];
