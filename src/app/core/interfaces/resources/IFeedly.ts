export interface IFeedlyCollectionFeed {
  description: string;
  feedId: string;
  id: string;
  language: 'en';
  subscribers: number;
  title: string;
  updated: number;
  velocity: number;
  website: string;
}

export interface IFeedlyCollection {
  created: number;
  customizable: boolean;
  enterprise: boolean;
  id: string;
  label: string; // category name
  numFeeds: number;
  feeds: IFeedlyCollectionFeed[];
}

export interface IFeedlyStreamItem {
  alternate: { href: string; type: string; }[];
  canonicalUrl: string;
  categories: { id: string; label: string; }[];
  crawled: number;
  engagement: number;
  engagementRate: number;
  fingerprint: string;
  fullContent: string;
  id: string;
  origin: { streamId: string, title: string, htmlUrl: string };
  originId: string;
  published: number;
  summary: { content: string, direction: string };
  title: string;
  unread: boolean;
  visual: { url: string };
  sponsored?: boolean;
}

export interface IFeedlyStream {
  continuation: string;
  id: string;
  updated: number;
  items: IFeedlyStreamItem[];
}

export interface IFeedlyStreamQueryParams {
  continuation?: string;
  streamId: string;
  backfill?: boolean;
  ranked?: string;
  count: number;
}
