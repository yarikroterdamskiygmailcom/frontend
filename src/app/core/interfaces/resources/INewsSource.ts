import { IResource, ResourceKey } from '../responses/IResource';

interface ISharedProps {
  categoryIds: string[];
  description: string;
  name: string;
  url: string;
}

export interface INewsSourceCreate extends ISharedProps { }

export interface INewsSourceBase extends ISharedProps, IResource<ResourceKey> { }

export interface INewsSourceDTO extends INewsSourceBase { }

export interface INewsSource extends INewsSourceDTO, IResource<ResourceKey> { }
