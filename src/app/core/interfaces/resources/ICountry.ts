import { IResource, ResourceKey } from '../responses/IResource';

interface ISharedProps {
  currencyIsoCode: string;
  euMember: boolean;
  isoAlpha2: string;
  isoAlpha3: string;
  isoNo: number;
  name: string;
}

export interface ICountryCreate extends ISharedProps { }

export interface ICountryBase extends ISharedProps, IResource<ResourceKey> { }

export interface ICountryDTO extends ICountryBase { }

export interface ICountry extends ICountryDTO, IResource<ResourceKey> { }
