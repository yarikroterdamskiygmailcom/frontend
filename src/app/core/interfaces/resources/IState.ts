import { IResource, ResourceKey } from '../responses/IResource';

interface ISharedProps {
  name: string;
  abbr: string;
}

export interface IStateCreate extends ISharedProps { }

export interface IStateBase extends ISharedProps, IResource<ResourceKey> { }

export interface IStateDTO extends IStateBase { }

export interface IState extends IStateDTO, IResource<ResourceKey> { }
