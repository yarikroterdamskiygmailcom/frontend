import { IResource, ResourceKey } from '../responses/IResource';

interface ISharedProps {
  category_id: (string | number)[];
}

export interface IBlogCreate extends ISharedProps { }

export interface IBlogBase extends ISharedProps, IResource<ResourceKey> {
  rate: number;
}

export interface IBlogDTO extends IBlogBase { }

export interface IBlog extends IBlogDTO, IResource<ResourceKey> { }
