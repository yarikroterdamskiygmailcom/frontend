import { IResource, ResourceKey } from '../responses/IResource';

interface ISharedProps {
  blogId: string;
  title: string;
  text: string;
  email: string;
  isFeatured: boolean;
}

export interface IGroupCreate extends ISharedProps { }

export interface IGroupBase extends ISharedProps, IResource<ResourceKey> {
  rate: number;
}

export interface IGroupDTO extends IGroupBase { }

export interface IGroup extends IGroupDTO, IResource<ResourceKey> { }
