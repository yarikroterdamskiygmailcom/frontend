import { IResource, ResourceKey } from '../responses/IResource';

interface ISharedProps {
  blogId: string;
  title: string;
  text: string;
  email: string;
  isFeatured: boolean;
}

export interface IPostCreate extends ISharedProps { }

export interface IPostBase extends ISharedProps, IResource<ResourceKey> {
  rate: number;
}

export interface IPostDTO extends IPostBase { }

export interface IPost extends IPostDTO, IResource<ResourceKey> { }
