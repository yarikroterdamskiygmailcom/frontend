export interface IRssFeed {
  author: string;
  description: string;
  image: string;
  link: string;
  title: string;
  url: string;
}

export interface IRssItem {
  author: string;
  categories: string[];
  content: string;
  description: string;
  enclosure: {};
  guid: string;
  link: string;
  pubDate: string;
  thumbnail: string;
  title: string;
}

export interface IRssSource {
  feed: IRssFeed;
  link: string;
  items: IRssItem[];
}
