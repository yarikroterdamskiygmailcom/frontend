export interface INews {
  title: string;
  text: string;
  date: number; // timestamp
  imageUrl: string;
  origin: string;
}
