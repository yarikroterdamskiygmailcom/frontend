import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject, of } from 'rxjs';
import { map, tap } from 'rxjs/operators';
// import { filter } from 'lodash';
import { IResource, ResourceKey } from '../../interfaces/responses/IResource';
import { TTL_SHORT, ResourceTrackerService } from '../../services/resource-tracker.service';
import { RepositoryService } from '../../services/repository.service';
import { IDataResponseBody } from '../../interfaces/responses/IDataResponse';
import { IResourceCollection } from '../../interfaces/responses/IResourceCollectin';
import { cleanUrl, urlConcat, filter } from '../../helpers/general.helper';
import { DataCarrier } from '../DataCarrier';
import { HttpGet } from '../http/http-get';
import { HttpPost } from '../http/http-post';
import { HttpDelete } from '../http/http-delete';
import { HttpPut } from '../http/http-put';
import { orderBy } from 'lodash';
export interface RepoParams<Entity extends IResource<ResourceKey>> {
  cloneMode?: boolean;
  useCache?: boolean;
  baseUrl?: string;
  prefixUrl?: number;
  errorHandler?: (err: HttpErrorResponse) => any;
  queryParams?: {};
  filter?: Partial<Entity>;
  isNotificationHidden?: boolean;
}

@Injectable()
export abstract class AbstractRepository<
  Entity extends IResource<ResourceKey>,
  DTO extends IResource<ResourceKey>,
  EntityCreate,
  ARepoParams extends RepoParams<Entity>
  > {
  protected streamSubject = new BehaviorSubject<Entity[]>([]);
  protected stream$: Observable<Entity[]> = this.streamSubject.asObservable();
  protected abstract resourceType: string;
  protected resourceTTL = TTL_SHORT;
  protected useCachePerDefault = false;
  protected resourceTracker: ResourceTrackerService;
  protected sortKey = 'id';
  protected sortAsce = false;

  public abstract getBaseUrl(repoParams: ARepoParams): string;

  protected abstract postProcess(data: DTO): Entity;

  constructor(
    protected repositoryService: RepositoryService,
  ) {
    this.repositoryService.authService
      .isLoggedIn$
      .subscribe(isLoggedIn => !isLoggedIn && this.clearStream());
    this.resourceTracker = repositoryService.resourceTracker;
  }

  private getExpiryDate(): Date {
    return new Date(Date.now() + this.resourceTTL);
  }

  public fetchAll(options: ARepoParams): Observable<Entity[]> {
    const url = cleanUrl(options.baseUrl || this.getBaseUrl(options));

    let source: Observable<Entity[]>;
    if (options.useCache || (options.useCache === undefined && this.useCachePerDefault)) {
      const cacheItem = this.resourceTracker.getCollection<Entity>(this.resourceType, url);
      if (cacheItem.isHit) {
        source = of(cacheItem.value);
      }
    }

    if (!source) {
      const request = new HttpGet(url, { init: { params: options.queryParams } });

      source = this.repositoryService.httpClient.request(request).pipe(
        map((response: HttpResponse<IDataResponseBody<IResourceCollection<DTO>>>) => response.body.data.items),
        map((items) => items.map((dto) => this.postProcess(dto))),
        map((items) => orderBy(items, [this.sortKey], this.sortAsce ? 'asc' : 'desc')),
        tap(entities => {
          this.resourceTracker.saveCollection(this.resourceType, entities, url, this.getExpiryDate(), this.resourceType);
        }),
        tap((entities) => options.queryParams ? this.addToStream(entities) : this.updateStream(entities)),
      );
    }

    return options && options.filter
    // TODO: replace this with lodash's filter maybe
      ? source.pipe(map(entities => filter(entities, options.filter)))
      : source;
  }

  public getStream(filterParams?: Partial<Entity>): Observable<Entity[]> {
    return filterParams
      ? this.stream$.pipe(map(entities => filter(entities, filterParams)))
      : this.stream$;
  }

  public getStreamValue(): Entity[] {
    return this.streamSubject.getValue();
  }

  public fetchOne(id: string, options: ARepoParams): Observable<Entity> {
    const baseUrl = options.baseUrl || this.getBaseUrl(options);
    const url = cleanUrl(urlConcat([baseUrl, id]));

    if (options.useCache || (options.useCache === undefined && this.useCachePerDefault)) {
      const cacheItem = this.resourceTracker.getOne<Entity>({ id }, this.resourceType);
      if (cacheItem.isHit) {
        return of(cacheItem.value);
      }
    }

    const request = new HttpGet(url, {
      entityId: id.toString(),
      cloneMode: options.cloneMode,
      init: {
        params: options.queryParams,
      }
    });


    if (options.errorHandler) {
      request.setErrorHandler(options.errorHandler);
    }

    const request$ = this.repositoryService.httpClient.request(request).pipe(
      map((responseBody: HttpResponse<IDataResponseBody<DTO>>) => responseBody.body.data),
      map((dto) => this.postProcess(dto)),
      tap(resource => this.resourceTracker.saveOne(resource, this.resourceType, this.getExpiryDate())),
      tap(entity => this.addToStream([entity])),
    );

    return request$;
  }

  public create(data: EntityCreate, options: ARepoParams): Observable<Entity> {
    const url = cleanUrl(options.baseUrl || this.getBaseUrl(options));
    const newData = { ...data as unknown as object };
    const request = new HttpPost<DataCarrier>(
      url,
      newData,
      {
        hideLoadingIndicator: options.isNotificationHidden,
        init: {
          params: options.queryParams,
        }
      },
    );

    return this.repositoryService.httpClient.request(request).pipe(
      map((res: HttpResponse<IDataResponseBody<DTO>>) => res.body.data),
      map((dto) => this.postProcess(dto)),
      tap(dto => {
        this.resourceTracker.deleteCollectionsByType(this.resourceType);
        this.resourceTracker.saveOne(dto, this.resourceType);
      }),
      tap(entity => this.addToStream([entity])),
    );
  }

  public update(resource: DTO, options: ARepoParams): Observable<Entity> {
    const baseUrl = options.baseUrl || this.getBaseUrl(options);
    const newResource = { ...resource } as DTO;
    delete newResource.id;
    const url = cleanUrl(urlConcat([baseUrl, resource.id]));
    const request = new HttpPut(
      url,
      newResource,
      {
        hideLoadingIndicator: options.isNotificationHidden,
        init: {
          params: options.queryParams,
        }
      }
    );

    return this.repositoryService.httpClient.request(request).pipe(
      map((res: HttpResponse<IDataResponseBody<DTO>>) => res.body.data),
      map((dto) => this.postProcess(dto)),
      tap(dto => {
        this.resourceTracker.deleteCollectionsByType(this.resourceType);
        this.resourceTracker.saveOne(dto, this.resourceType);
      }),
      tap(entity => this.addToStream([entity]))
    );
  }

  public delete(id: string, options: ARepoParams): Observable<void> {
    const baseUrl = options.baseUrl || this.getBaseUrl(options);
    const url = cleanUrl(urlConcat([baseUrl, id]));
    const request = new HttpDelete(
      url,
      {
        hideLoadingIndicator: options.isNotificationHidden,
        init: {
          params: options.queryParams,
        }
      }
    );
    return this.repositoryService.httpClient.request<void>(request).pipe(
      tap(() => this.removeFromStream(id)),
      tap(dto => {
        this.resourceTracker.deleteCollectionsByType(this.resourceType);
        this.resourceTracker.deleteOne({ id }, this.resourceType);
      }),
      map(() => undefined)
    );
  }

  protected addToStream = (entities: Entity[]): void => {
    let stream = this.streamSubject.getValue();
    entities.forEach(entity => {
      const isNew = !stream.find(ad => ad.id === entity.id);
      stream = isNew
        ? orderBy([...stream, entity], [this.sortKey], this.sortAsce ? 'asc' : 'desc')
        : stream.map(e => (e.id === entity.id ? entity : e));
    });
    this.streamSubject.next(stream);
  }

  protected updateStream = (entities: Entity[]): void => {
    this.streamSubject.next(entities);
  }

  public removeFromStream(id: string) {
    const streamItems = this.streamSubject.getValue();
    const nextStreamItems = streamItems.filter(i => i.id !== id);
    this.streamSubject.next(nextStreamItems);
  }

  public clearStream() {
    this.streamSubject.next([]);
  }

  public getPropertyById = (id: string, property: string): string => {
    const foundItem = this.getStreamValue()
      .find(item => item.id === id);
    return foundItem
      ? foundItem[property]
      : id;
  }
}
