import { AppException } from './AppException';

export class InvalidArgumentException extends AppException {
}
