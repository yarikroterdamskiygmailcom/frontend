export class AppException extends Error {
  constructor(m: string) {
    super(m);
    Object.setPrototypeOf(this, AppException.prototype);
  }
}
