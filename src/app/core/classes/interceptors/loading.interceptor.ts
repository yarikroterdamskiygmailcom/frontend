import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpEventType } from '@angular/common/http';
import { Observable } from 'rxjs';
import { skipWhile, finalize } from 'rxjs/operators';
import { LoadingIndicatorService } from '../../services/loading-indicator.service';
import { RequestsService  } from '../../services/requests.service';
import { EHttpRequest } from '../../classes/http/EHttpRequest';

@Injectable()
export class LoadingInterceptor implements HttpInterceptor {

  constructor(
    private loadingService: LoadingIndicatorService,
    private requestsService: RequestsService,
  ) { }

  public intercept(request: EHttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (request.url.includes('assets') || request?.options?.hideLoadingIndicator){
      return next.handle(request);
    }
    this.requestsService.addRequest(request);
    this.loadingService.isLoadingSubject.next(true);

    return next.handle(request).pipe(
      finalize(() => this.requestsService.removeRequest(request)),
      skipWhile((event: HttpEvent<any>) => event.type === HttpEventType.Sent),
    );
  }
}
