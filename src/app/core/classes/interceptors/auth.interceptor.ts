import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpEventType, HttpRequest, HttpErrorResponse } from '@angular/common/http';
import { AuthService } from '../../../authentication/services/auth.service';
import { Observable, throwError } from 'rxjs';
import { skipWhile, catchError } from 'rxjs/operators';
import { EHttpRequest } from '../http/EHttpRequest';
import { environment } from '../../../../environments/environment';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor(
    private authService: AuthService,
  ) { }

  public intercept(request: EHttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (!request.url.includes('http') && !request.url.includes('assets')) {
      return next.handle(this.addHeaders(request)).pipe(
        skipWhile((event: HttpEvent<any>) => event && event.type === HttpEventType.Sent),
        catchError((response: HttpErrorResponse) => {

          return throwError(response);
        })
      );
    } else {
      return next.handle(request);
    }
  }

  public addHeaders(request: EHttpRequest<any>): EHttpRequest<any> | HttpRequest<any> {
    const cleanedUrl = request.url.replace(/\/\/+/g, '/');
    const idToken: string = this.authService.getIdToken();
    const headers = {
      Authorization: idToken,
      Accept: request.headers.get('accept') || 'application/json',
      ...((typeof request.getAdditionalHeaders === 'function') ? request.getAdditionalHeaders() : {})
    };


    return request.noModify
      ? request
      : request.cloneWithEHttp({
        url: `${environment.apiHost}${cleanedUrl}`,
        withCredentials: true,
        setHeaders: headers,
        instance: request,
      });
  }
}
