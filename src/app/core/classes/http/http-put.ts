import { EHttpRequest, RequestOptions, getHttpPrams, getHttpHeaders } from './EHttpRequest';

export class HttpPut<T> extends EHttpRequest<T> {
  constructor(url: string, body, options: RequestOptions = {}) {
    const init = {
      ...options.init,
      params: getHttpPrams(options.init ? options.init.params : {}),
      headers: getHttpHeaders(options.init ? options.init.headers : {}),
    };
    super('PUT', url, body, init);
    this.setAdditionalHeaders(options.init ? options.init.headers : {});
    this.initOptions(options);
  }
}
