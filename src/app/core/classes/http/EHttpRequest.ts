import { HttpRequest, HttpParams, HttpHeaders } from '@angular/common/http';

export const getHttpPrams = (
  paramsObj: { [key: string]: string | number | [] } = {}
): HttpParams => {
  let httpParams = new HttpParams();
  Object.entries(paramsObj).forEach(([key, val]) => {
    if (Array.isArray(val)) {
      val.forEach(v => httpParams = httpParams.append(key, v));
    } else {
      httpParams = httpParams.set(key, val && val.toString());
    }
  });
  return httpParams;
};

export const getHttpHeaders = (
  headersObj: { [key: string]: string | number | [] } = {}
): HttpHeaders => {
  let httpHeaders = new HttpHeaders();
  Object.entries(headersObj).forEach(([key, val]) => {
    if (Array.isArray(val)) {
      val.forEach(v => httpHeaders = httpHeaders.append(key, v));
    } else {
      httpHeaders = httpHeaders.set(key, val && val.toString());
    }
  });
  return httpHeaders;
};

export interface RequestOptions {
  entityId?: string;
  hideLoadingIndicator?: boolean;
  cloneMode?: boolean;
  init?: {
    headers?: {};
    reportProgress?: boolean;
    params?: {};
    responseType?: 'arraybuffer' | 'blob' | 'json' | 'text';
    withCredentials?: boolean;
    observe?: string;
  };
}

export class EHttpRequest<T> extends HttpRequest<T> {
  private errorHandler?: (HttpResponseError) => any;
  public noModify?: boolean;
  private additionalHeaders: object = {};
  private entityId?: string;
  private isNotificationHidden: boolean;
  public options: RequestOptions;

  /**
   * This method is needed to save passed options, so when we clone this object we could
   * access to the options in the new instance
   */
  public initOptions(options: RequestOptions = {}) {
    this.options = options;
    if (options.hideLoadingIndicator) {
      this.setNotificationVisibility(options.hideLoadingIndicator);
    }
    if (options.entityId) {
      this.setEntityId(options.entityId);
    }
  }

  public setErrorHandler(errorHandler: (HttpResponseError) => any) {
    this.errorHandler = errorHandler;
  }

  public getErrorHander(): undefined | ((HttpResponseError) => any) {
    return this.errorHandler;
  }

  public setNoModify() {
    this.noModify = true;
  }

  public appendAdditionalHeader(key: string, value: string) {
    this.additionalHeaders[key] = value;
  }

  public setAdditionalHeaders(headers: object) {
    this.additionalHeaders = headers || {};
  }

  public getAdditionalHeaders() {
    return this.additionalHeaders;
  }

  protected setEntityId(id: string) {
    this.entityId = id;
  }

  public getEntityId(): string {
    return this.entityId;
  }

  /**
   * When we use 'req.clone' in AuthInterceptor it creates new instance of HttpRequest and
   * in the next interceptor we are unable to use methods and properties of EHttpRequest,
   * this method creates instance of EHttpRequest instead of HttpRequest, so we would be
   * able to use all methods and properties of EHttpRequest in all interceptors
   */
  public cloneWithEHttp(update: {
    headers?: HttpHeaders;
    reportProgress?: boolean;
    params?: HttpParams;
    responseType?: 'arraybuffer' | 'blob' | 'json' | 'text';
    withCredentials?: boolean;
    body?: T | null;
    method?: string;
    url?: string;
    setHeaders?: {
        [name: string]: string | string[];
    };
    setParams?: {
        [param: string]: string;
    };
    instance?: EHttpRequest<T>,
  }): EHttpRequest<T> {
    const clone = super.clone(update);
    const newInstance = new EHttpRequest(
      clone.method as any,
      clone.url,
      clone.body,
      {
        params: clone.params,
        headers: clone.headers,
        reportProgress: clone.reportProgress,
        responseType: clone.responseType,
        withCredentials: clone.withCredentials
      }
    );
    newInstance.initOptions(update.instance.options);
    return newInstance;
  }

  protected setNotificationVisibility(isHidden: boolean) {
    this.isNotificationHidden = isHidden;
  }

  public shouldHideNotification = (): boolean => {
    return this.isNotificationHidden;
  }
}
