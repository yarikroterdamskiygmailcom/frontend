import { FormGroup } from '@angular/forms';

export const validateAllFormFields = (formGroup: FormGroup, opts: { onlySelf?: boolean, emitEvent?: boolean } = {} ): void => {
  for (const control in formGroup.controls) {
    if (formGroup.controls.hasOwnProperty(control)) {
      const formControl = formGroup.controls[control];
      if (!formControl.valid) {
        formControl.markAsDirty(opts);
        formControl.markAsTouched(opts);
        formControl.updateValueAndValidity(opts);
      }
    }
  }
};
