import { IOption } from '../interfaces/IOption';

export const urlConcat = (urlSegments: (string | number)[]): string => {
  let finalUrl = '';

  urlSegments.forEach(s => {
    const segment = s.toString(); // so numbers can be valid arguments
    const toAdd = segment.slice(-1) === '/' ? segment : segment + '/';
    finalUrl += toAdd;
  });

  return finalUrl;
};

export const withTrailingSlash = (url: string) => {
  if (url.substr(url.length - 1) === '/') { return url; }
  return url += '/';
};

export const cleanUrl = (originalUrl: string): string => {
  let url = originalUrl.slice(-1) === '/' ? originalUrl.slice(0, -1) : originalUrl;
  url = url.replace(/\/\//g, '/');
  return url;
};

export const openNewTab = (url: string): void => {
  window.open(url, '_blank');
};

export const filter = (array: any[], filters: object): any[] => {
  const filterKeys = Object.keys(filters);
  return array.filter(item => {
    return filterKeys.every(key => {
      return typeof filters[key] === 'function'
        ? filters[key](item)
        : filters[key] === item[key];
    });
  });
};

/**
 * used to extract text content from html string
 */
export const extractContent = (htmlString: string): string => {
  const span = document.createElement('span');
  span.innerHTML = htmlString;
  return span.textContent || span.innerText;
};


export const enumToArray = (object: {}): IOption[] => {
  if (typeof object !== 'object') {
    return [];
  }
  return Object.keys(object)
    .map((_: string): IOption => ({
      label: object[_],
      value: object[_],
    }));
};

export const getRandomInteger = (min: number, max: number): number => {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min;
};
