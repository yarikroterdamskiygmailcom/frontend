import { NgModule } from '@angular/core';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule, MatIconRegistry } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { MatMenuModule } from '@angular/material/menu';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatBadgeModule } from '@angular/material/badge';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatCardModule } from '@angular/material/card';
import { DomSanitizer } from '@angular/platform-browser';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatSelectModule } from '@angular/material/select';
import { MatBottomSheetModule } from '@angular/material/bottom-sheet';
import { MatDatepickerModule } from '@angular/material/datepicker';
import {MatNativeDateModule} from "@angular/material/core";
import {MatAutocompleteModule} from "@angular/material/autocomplete";

export const materialImports = [
  MatToolbarModule,
  MatButtonModule,
  MatSidenavModule,
  MatIconModule,
  MatListModule,
  MatMenuModule,
  MatFormFieldModule,
  MatInputModule,
  MatBadgeModule,
  MatButtonToggleModule,
  MatGridListModule,
  MatCardModule,
  MatSnackBarModule,
  MatSelectModule,
  MatBottomSheetModule,
  MatDatepickerModule,
  MatNativeDateModule,
  MatAutocompleteModule,
];

const mchSvgIcons: string[] = [
  'message',
  'tweets',
  'blogs',
  'news',
  'search',
  'add',
  'community',
  'arrow',
  'rating-full',
  'rating-full-gray',
  'check',
  'upload',
  'trash',
  'red-star',
  'grey-star',
];

@NgModule({
  declarations: [],
  imports: [
    ...materialImports
  ],
  providers: [
    MatDatepickerModule
  ],
  exports: [
    ...materialImports
  ]
})
export class MaterialModule {
  constructor(
    private iconRegistry: MatIconRegistry,
    private sanitizer: DomSanitizer,
  ) {
    mchSvgIcons.forEach(icon => {
      this.iconRegistry.addSvgIcon(
        icon,
        this.sanitizer.bypassSecurityTrustResourceUrl(`assets/icons/${icon}.svg`)
      );
    });
  }
}
