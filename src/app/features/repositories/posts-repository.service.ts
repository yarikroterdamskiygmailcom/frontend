import { Injectable } from '@angular/core';
import { AbstractRepository, RepoParams } from '../../core/classes/abstracts/abstract.repository';
import { RepositoryService } from '../../core/services/repository.service';
import { IPost, IPostDTO, IPostCreate } from '../../core/interfaces/resources/IPost';
import { HttpSuffix, ResourceType } from './repository-constants';

@Injectable({
  providedIn: 'root'
})
export class PostsRepositoryService extends AbstractRepository<
IPost,
IPostDTO,
IPostCreate,
RepoParams<IPost>
> {
  protected resourceType = ResourceType.Post;

  constructor(repositoryService: RepositoryService) {
    super(repositoryService);
  }

  public getBaseUrl(): string {
    return HttpSuffix.Post;
  }

  public postProcess(item: IPostDTO): IPost {
    return item;
  }
}
