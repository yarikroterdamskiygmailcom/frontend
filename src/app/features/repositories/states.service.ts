import { Injectable } from '@angular/core';
import { HttpSuffix, ResourceType } from './repository-constants';
import { AbstractRepository, RepoParams } from '../../core/classes/abstracts/abstract.repository';
import { IState, IStateDTO, IStateCreate } from '../../core/interfaces/resources/IState';
import { RepositoryService } from '../../core/services/repository.service';
@Injectable({
  providedIn: 'root'
})
export class StatesService extends AbstractRepository<
IState,
IStateDTO,
IStateCreate,
RepoParams<IState>
> {
  protected resourceType = ResourceType.State;

  constructor(repositoryService: RepositoryService) {
    super(repositoryService);
  }

  getBaseUrl = () => HttpSuffix.States;

  public postProcess = (item: IStateDTO): IState => item;
}
