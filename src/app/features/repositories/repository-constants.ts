export enum ResourceType {
  Blog = 'Blog',
  Category = 'Category',
  NewsSource = 'NewsSource',
  Post = 'Post',
  State = 'State',
  Country = 'Country',
  User = 'User',
}

export enum HttpSuffix {
  Blogs = '/blogs',
  Post = '/posts',
  Categories = '/categories',
  NewsSource = '/news-sources',
  Feedly = '/feedly',
  States = '/states',
  Countries = '/countries',
  Users = '/users',
}
