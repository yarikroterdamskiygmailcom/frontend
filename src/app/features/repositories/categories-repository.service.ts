import { Injectable } from '@angular/core';
import { RepositoryService } from '../../core/services/repository.service';
import { AbstractRepository, RepoParams } from '../../core/classes/abstracts/abstract.repository';
import { ICategory, ICategoryDTO, ICategoryCreate } from '../../core/interfaces/resources/ICategory';
import { HttpSuffix, ResourceType } from './repository-constants';

@Injectable({
  providedIn: 'root'
})
export class CategoriesRepositoryService extends AbstractRepository<
ICategory,
ICategoryDTO,
ICategoryCreate,
RepoParams<ICategory>
> {
  protected resourceType = ResourceType.Category;
  sortAsce = true;
  sortKey = 'name';

  constructor(repositoryService: RepositoryService) {
    super(repositoryService);
  }

  getBaseUrl = () => HttpSuffix.Categories;

  public postProcess = (item: ICategoryDTO): ICategory => item;
}
