import { Injectable } from '@angular/core';
import { HttpSuffix, ResourceType } from './repository-constants';
import { AbstractRepository, RepoParams } from '../../core/classes/abstracts/abstract.repository';
import { IUser, IUserDTO, IUserCreate } from '../../core/interfaces/resources/IUser';
import { RepositoryService } from '../../core/services/repository.service';
@Injectable({
  providedIn: 'root'
})
export class UsersService extends AbstractRepository<
IUser,
IUserDTO,
IUserCreate,
RepoParams<IUser>
> {
  protected resourceType = ResourceType.User;

  constructor(repositoryService: RepositoryService) {
    super(repositoryService);
  }

  getBaseUrl = () => HttpSuffix.Users;

  public postProcess = (item: IUserDTO): IUser => item;
}
