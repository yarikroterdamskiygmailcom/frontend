import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { IFeedlyStream, IFeedlyCollection, IFeedlyStreamQueryParams } from '../../core/interfaces/resources/IFeedly';
import { map } from 'rxjs/operators';
import { ICategory } from '../../core/interfaces/resources/ICategory';
import { Observable } from 'rxjs';
import { HttpGet } from 'src/app/core/classes/http/http-get';
import { IDataResponseBody } from 'src/app/core/interfaces/responses/IDataResponse';
import { IResourceCollection } from 'src/app/core/interfaces/responses/IResourceCollectin';
import { HttpSuffix } from './repository-constants';

@Injectable({
  providedIn: 'root'
})
export class FeedlyRepositoryService {
  private readonly apiBase = HttpSuffix.Feedly;
  constructor(
    private http: HttpClient
  ) { }

  public fetchCollections(category: ICategory): Observable<IFeedlyCollection[]> {
    const req = new HttpGet(`${this.apiBase}/collections`, { hideLoadingIndicator: true });
    return this.http.request(req)
      .pipe(
        map((res: HttpResponse<IDataResponseBody<IResourceCollection<IFeedlyCollection>>>) => res.body.data.items),
      );
  }

  public fetchStreams(streamId: string, count = 10, continuation: string): Observable<IFeedlyStream> {
    const params: IFeedlyStreamQueryParams = {
      streamId,
      count,
    };
    if (continuation.length) {
      params.continuation = continuation;
    }
    const req = new HttpGet(`${this.apiBase}/streams`, {
      init: { params },
      hideLoadingIndicator: true
    });
    return this.http.request(req)
      .pipe(
        map((res: HttpResponse<IDataResponseBody<IFeedlyStream>>) => {
          return res.body.data;
        })
      );
  }
}
