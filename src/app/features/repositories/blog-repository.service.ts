import { Injectable } from '@angular/core';
import { AbstractRepository, RepoParams } from '../../core/classes/abstracts/abstract.repository';
import { RepositoryService } from '../../core/services/repository.service';
import { IBlog, IBlogCreate, IBlogDTO } from '../../core/interfaces/resources/IBlog';
import { HttpSuffix, ResourceType } from './repository-constants';

@Injectable({
  providedIn: 'root'
})
export class BlogRepositoryService extends AbstractRepository<
IBlog,
IBlogDTO,
IBlogCreate,
RepoParams<IBlog>
> {
  protected resourceType = ResourceType.Blog;

  constructor(repositoryService: RepositoryService) {
    super(repositoryService);
  }

  public getBaseUrl(): string {
    return HttpSuffix.Blogs;
  }

  public postProcess(item: IBlogDTO): IBlog {
    return item;
  }

}
