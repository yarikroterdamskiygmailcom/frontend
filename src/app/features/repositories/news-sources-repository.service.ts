import { Injectable } from '@angular/core';
import { AbstractRepository, RepoParams } from 'src/app/core/classes/abstracts/abstract.repository';
import { INewsSource, INewsSourceDTO, INewsSourceCreate } from 'src/app/core/interfaces/resources/INewsSource';
import { RepositoryService } from 'src/app/core/services/repository.service';
import { Observable } from 'rxjs';
import { IRssSource } from 'src/app/core/interfaces/resources/IRssSource';
import { HttpSuffix, ResourceType } from './repository-constants';

@Injectable({
  providedIn: 'root'
})
export class NewsSourcesRepositoryService extends AbstractRepository<
INewsSource,
INewsSourceDTO,
INewsSourceCreate,
RepoParams<INewsSource>
> {
  protected resourceType = ResourceType.NewsSource;
  private rssToJsonServiceBaseUrl = 'https://rss2json.com/api.json?rss_url=';

  constructor(repositoryService: RepositoryService) {
    super(repositoryService);
  }

  getBaseUrl = () => HttpSuffix.NewsSource;

  public postProcess = (item: INewsSourceDTO): INewsSource => item;

  /**
   *
   * @param url rss url
   */
  public fetchNewsByUrl(url: string): Observable<IRssSource> {
    return this.repositoryService.httpClient
      .get<IRssSource>(this.rssToJsonServiceBaseUrl + url);
  }
}
