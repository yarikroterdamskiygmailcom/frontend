import { Injectable } from '@angular/core';
import { HttpSuffix, ResourceType } from './repository-constants';
import { ICountry, ICountryDTO, ICountryCreate } from '../../core/interfaces/resources/ICountry';
import { RepoParams, AbstractRepository } from '../../core/classes/abstracts/abstract.repository';
import { RepositoryService } from '../../core/services/repository.service';
@Injectable({
  providedIn: 'root'
})
export class CountriesService extends AbstractRepository<
ICountry,
ICountryDTO,
ICountryCreate,
RepoParams<ICountry>
> {
  protected resourceType = ResourceType.Category;
  public sortKey = 'name';
  constructor(repositoryService: RepositoryService) {
    super(repositoryService);
  }

  getBaseUrl = () => HttpSuffix.Countries;

  public postProcess = (item: ICountryDTO): ICountry => item;
}
