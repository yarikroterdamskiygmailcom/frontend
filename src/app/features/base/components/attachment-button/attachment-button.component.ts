import {
  ChangeDetectionStrategy,
  Component, ElementRef,
  EventEmitter,
  Output,
  ViewChild,
} from '@angular/core';

@Component({
  selector: 'app-attachment-button',
  templateUrl: './attachment-button.component.html',
  styleUrls: ['./attachment-button.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AttachmentButtonComponent {
  @Output() public attach: EventEmitter<boolean> = new EventEmitter<boolean>();

  @ViewChild('file') file: ElementRef;

  public click() {
    this.file.nativeElement.click();
    this.attach.emit(true);
  }
}
