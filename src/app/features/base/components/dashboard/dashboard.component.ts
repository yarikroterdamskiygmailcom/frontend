import { Component, OnInit, ChangeDetectionStrategy, OnDestroy } from '@angular/core';
import { Subject, Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { IPost } from '../../../../core/interfaces/resources/IPost';
import { LoadingIndicatorService } from 'src/app/core/services/loading-indicator.service';
import { BLOGS_MOCK } from 'src/app/core/data/mock/blogs.mock';
import { enumToArray } from 'src/app/core/helpers/general.helper';
import { IOption } from 'src/app/core/interfaces/IOption';

enum DASHBOARD_VIEWS {
  POSTS = 'Blogs',
  TWEETS = 'Tweets',
  NEWS = 'News'
}

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DashboardComponent implements OnInit, OnDestroy {
  public posts$: Observable<IPost[]>;
  private unsubscriber = new Subject<null>();
  public DASHBOARD_VIEWS = DASHBOARD_VIEWS;
  public selectedView: string = DASHBOARD_VIEWS.POSTS;
  public dashboardItems: IOption[] = enumToArray(DASHBOARD_VIEWS);
  public sectionTitle = 'Home';
  constructor(
    private breakpointObserver: BreakpointObserver,
    public loadingService: LoadingIndicatorService,
  ) {
  }

  public isHandset$: Observable<boolean> = this.breakpointObserver
    .observe(Breakpoints.Handset)
    .pipe(map(result => result.matches));


  ngOnInit(): void {
    this.fetchPosts();
  }

  private fetchPosts(): void {
    this.posts$ = of(BLOGS_MOCK);
  }

  ngOnDestroy() {
    this.unsubscriber.next(null);
  }

  public onViewChange(value: string) {
    this.selectedView = value;
    switch (this.selectedView) {
      case DASHBOARD_VIEWS.NEWS:
        this.sectionTitle = 'News';
        break;
      case DASHBOARD_VIEWS.POSTS:
        this.sectionTitle = 'Blogs';
        break;
      case DASHBOARD_VIEWS.TWEETS:
        this.sectionTitle = 'Tweets';
        break;
      default:
        this.sectionTitle = 'Home';
    }
  }

}
