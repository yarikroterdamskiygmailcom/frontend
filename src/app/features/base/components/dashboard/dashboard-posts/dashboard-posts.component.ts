import { Component, OnInit, ChangeDetectionStrategy, Input } from '@angular/core';
import { IPost } from 'src/app/core/interfaces/resources/IPost';

@Component({
  selector: 'app-dashboard-posts',
  templateUrl: './dashboard-posts.component.html',
  styleUrls: ['./dashboard-posts.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DashboardPostsComponent implements OnInit {
  @Input() public posts: IPost[];
  constructor() { }

  ngOnInit(): void {  }

}
