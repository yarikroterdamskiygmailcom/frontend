import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'app-dashboard-tweets',
  templateUrl: './dashboard-tweets.component.html',
  styleUrls: ['./dashboard-tweets.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DashboardTweetsComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
