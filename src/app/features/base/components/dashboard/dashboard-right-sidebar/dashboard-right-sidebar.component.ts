import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'app-dashboard-right-sidebar',
  templateUrl: './dashboard-right-sidebar.component.html',
  styleUrls: ['./dashboard-right-sidebar.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DashboardRightSidebarComponent implements OnInit {
  ngOnInit() { }

}
