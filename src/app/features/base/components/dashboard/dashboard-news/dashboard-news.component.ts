import { Component, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'app-dashboard-news',
  templateUrl: './dashboard-news.component.html',
  styleUrls: ['./dashboard-news.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DashboardNewsComponent { }
