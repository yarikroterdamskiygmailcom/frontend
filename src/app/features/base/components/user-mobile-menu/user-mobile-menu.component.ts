import { ChangeDetectionStrategy, Component } from '@angular/core';
import { AuthService } from '../../../../authentication/services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-user-bottom-sheet',
  templateUrl: './user-mobile-menu.component.html',
  styleUrls: ['./user-mobile-menu.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class UserMobileMenuComponent {
  constructor(
    private authService: AuthService,
    private readonly router: Router,
  ) { }

  public logout(): void {
    this.authService.logout()
      .subscribe(() => {
        this.router.navigate(['/login']);
      });
  }

  public navigate(path: string): void {
    this.router.navigate([path]);
  }
}
