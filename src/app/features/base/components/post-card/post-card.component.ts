import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

@Component({
  selector: 'app-post-card',
  templateUrl: './post-card.component.html',
  styleUrls: ['./post-card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PostCardComponent {
  // TODO: Add type once we are connected to backend
  @Input()
  public post: any;
  @Input() isFeatureBlogPost: boolean = false;

}
