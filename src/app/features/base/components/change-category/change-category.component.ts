import { ChangeDetectionStrategy, Component, Inject } from '@angular/core';
import { MAT_BOTTOM_SHEET_DATA, MatBottomSheetRef } from '@angular/material/bottom-sheet';
import { ICategory } from '../../../../core/interfaces/resources/ICategory';

@Component({
  selector: 'app-change-category',
  templateUrl: './change-category.component.html',
  styleUrls: ['./change-category.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ChangeCategoryComponent {
  constructor(
      @Inject(MAT_BOTTOM_SHEET_DATA) public item: ICategory,
      private _bottomSheetRef: MatBottomSheetRef<ChangeCategoryComponent>
  ) {}

  closeBottomSheet() :void{
    this._bottomSheetRef.dismiss();
  }
}
