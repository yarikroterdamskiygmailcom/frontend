import { Component, ChangeDetectionStrategy } from '@angular/core';
import { LoadingIndicatorService } from '../../../../core/services/loading-indicator.service';

@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LandingComponent  {
  public landingItems = [
    {
      title: 'My Sanctuary',
      description: 'Interface for privacy with patient, family and friends',
      link: './../../../../../assets/landing-icons/sanctuary-card.svg'
    },
    {
      title: 'Community',
      description: 'Interface for interaction with peers, news, opinion polls, navigation and outreach',
      link: './../../../../../assets/landing-icons/community-card.svg'
    },
    {
      title: 'Experts',
      description: 'Interface with healthcare professionals',
      link: './../../../../../assets/landing-icons/expert-card.svg'
    },
  ];
  constructor(
    public loadingService: LoadingIndicatorService,
  ) { }

}
