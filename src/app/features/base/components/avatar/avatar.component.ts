import { Component, OnInit, ChangeDetectionStrategy, Input } from '@angular/core';
import { Observable } from 'rxjs';
import { AuthService } from '../../../../authentication/services/auth.service';

@Component({
  selector: 'app-avatar',
  templateUrl: './avatar.component.html',
  styleUrls: ['./avatar.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AvatarComponent implements OnInit {
  public user$: Observable<firebase.User>;
  @Input() public size = 16;

  constructor(
    private authService: AuthService,
  ) { }

  ngOnInit() {
    this.user$ = this.authService.getUser();
  }

  initials(name: string | undefined) {
    if (typeof name === 'string') {
      const arr = name.split(' ');
      const firstWord = arr[0];
      const lastWord = arr[1];
      const firstLetter = (firstWord && firstWord[0]) || '_';
      const lastLetter = (lastWord ? lastWord[0] : firstWord && firstWord[2]) || '_';
      return `${firstLetter.toUpperCase()}${lastLetter.toUpperCase()}`;
    }
  }
}
