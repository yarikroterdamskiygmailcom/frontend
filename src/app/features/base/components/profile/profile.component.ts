import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { isObservable, Observable, of, forkJoin } from 'rxjs';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { map, startWith, tap, switchMap, catchError, take } from 'rxjs/operators';
import { LoadingIndicatorService } from 'src/app/core/services/loading-indicator.service';
import { ICountry } from 'src/app/core/interfaces/resources/ICountry';
import { IState } from 'src/app/core/interfaces/resources/IState';
import { CountriesService } from 'src/app/features/repositories/countries.service';
import { StatesService } from 'src/app/features/repositories/states.service';
import { GENDER, IUser, genderItems, rolesItems } from 'src/app/core/interfaces/resources/IUser';
import { CategoriesRepositoryService } from 'src/app/features/repositories/categories-repository.service';
import { ICategory } from 'src/app/core/interfaces/resources/ICategory';
import { UsersService } from 'src/app/features/repositories/users.service';
import { AuthService } from 'src/app/authentication/services/auth.service';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProfileComponent implements OnInit {
  public genderItems = genderItems;
  public rolesItems = rolesItems;
  public form: FormGroup;
  public selectedGender: GENDER;
  public countries: ICountry[] = [];
  public states: IState[] = [];
  public categories: ICategory[] = [];
  public filteredCountries$: Observable<ICountry[]> = of([]);
  public filteredStates$: Observable<IState[]> = of([]);
  public GENDER = GENDER;
  private user: IUser;

  constructor(
    public loadingService: LoadingIndicatorService,
    private countriesService: CountriesService,
    private statesService: StatesService,
    private categoriesService: CategoriesRepositoryService,
    private usersRepo: UsersService,
    private fb: FormBuilder,
    private authService: AuthService,
  ) { }

  ngOnInit(): void {
    forkJoin([
      this.countriesService.fetchAll({ useCache: true }),
      this.statesService.fetchAll({ useCache: true }),
      this.categoriesService.fetchAll({ useCache: true }),
      this.authService.getUser()
        .pipe(
          take(1),
          switchMap(user => {
            return this.usersRepo.fetchOne(user.email, { errorHandler: () => true })
              .pipe(
                catchError((err: HttpErrorResponse) => {
                  if (err.status === 404) {
                    return of({
                      id: user.email,
                    } as IUser);
                  }
                })
              );
          })
        ),
    ]).pipe(
      tap(([countries, states, categories, user]) => {
        this.countries = countries;
        this.states = states;
        this.categories = categories;
        this.user = user;
        this.initForm(user);
        this.setCountryFilter();
        this.initStateFilter();
      })
    ).subscribe();
  }

  private initForm(user: IUser = { picture: '' } as IUser) {
    this.form = this.fb.group({
      id: [user.id],
      firstName: [user.firstName, [Validators.required, Validators.minLength(3), Validators.maxLength(50)]],
      lastName: [user.lastName, [Validators.required, Validators.minLength(3), Validators.maxLength(50)]],
      displayName: [user.displayName, [Validators.required, Validators.minLength(3), Validators.maxLength(50)]],
      categoryId: [user.categoryId, [Validators.required]],
      dateOfBirth: [user.dateOfBirth, [Validators.required]],
      gender: [user.gender, [Validators.required]],
      picture: ['', []],
      role: [user.role, [Validators.required]],
      tel: [user.tel, [Validators.required]],
      about: [user.about, [Validators.required, Validators.minLength(3), Validators.maxLength(60)]],
      // address
      countryId: [user.countryId, [Validators.required]],
      stateId: [user.stateId, [Validators.required]],
      city: [user.city, [Validators.required, Validators.minLength(3), Validators.maxLength(30)]],
      zip: [user.zip, [Validators.required]],
    });
    this.onGenderSelect(user.gender);
  }

  private _filter<T>(
    array: T[],
    value: string
  ): T[] {
    if (!array || !(array && array.length)) {
      return [];
    }
    const filterValue = value.toLowerCase();
    return array.filter((state: any): boolean => state.name.toLowerCase().includes(filterValue));
  }

  private getObservableFiltered<T>(
    valueChanges: Observable<string>,
    defaultValue: string = '',
    array: T[]
  ): Observable<T[]> {
    if (isObservable(valueChanges)) {
      return valueChanges.pipe(
        startWith(defaultValue),
        map((name: string) => {
          return name
            ? this._filter<T>(array, name)
            : array.slice();
        })
      );
    } else {
      return of([]);
    }
  }

  private setCountryFilter(): void {
    const countryCtrl = this.form.get('countryId');
    this.filteredCountries$ = this.getObservableFiltered<ICountry>(
      countryCtrl.valueChanges,
      '',
      this.countries
    );
    countryCtrl.valueChanges
      .subscribe(value => {
        if (!this.states.length && value === 'United States') {
          const stateCtrl = this.form.get('stateId');
          stateCtrl.disable();
          stateCtrl.patchValue(null);
        }
      });
  }

  private initStateFilter(): void {
    this.filteredStates$ = this.getObservableFiltered<IState>(
      this.form.controls.stateId.valueChanges,
      '',
      this.states
    );
  }

  public submit() {
    if (!this.form.valid) {
      return;
    }
    const value = this.form.getRawValue() as IUser;

    this.user.firstName
      ? this.usersRepo.update({ ...value }, {})
        .subscribe(user => this.afterSubmit(user))
      : this.usersRepo.create({ ...value }, {})
        .subscribe(user => this.afterSubmit(user));
  }

  private afterSubmit(user: IUser) {
    this.user = user;
    this.initForm(user);
  }

  public countryDisplay = (countryId: string) => {
    const country = this.countriesService.getStreamValue()
      .find(c => c.id === countryId);
    return country?.name;
  }

  public stateDisplay = (stateId: string) => {
    const state = this.statesService.getStreamValue()
      .find(c => c.id === stateId);
    return state?.name;
  }

  public onGenderSelect(gender: GENDER) {
    this.selectedGender = gender;
    this.form.get('gender').patchValue(gender);
  }
}
