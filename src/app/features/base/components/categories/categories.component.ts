import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { LoadingIndicatorService } from '../../../../core/services/loading-indicator.service';
import { CategoriesRepositoryService } from '../../../repositories/categories-repository.service';
import { ICategory } from '../../../../core/interfaces/resources/ICategory';
import { BottomSheetService, NameInstance } from '../../services/bottom.sheet.service';
import { ChangeCategoryComponent } from '../change-category/change-category.component';
import { UsersService } from 'src/app/features/repositories/users.service';
import { AuthService } from 'src/app/authentication/services/auth.service';
import { forkJoin } from 'rxjs';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CategoriesComponent implements OnInit {
  public categories: ICategory[];
  public selectedCategoryId: string;
  constructor(
    public loadingService: LoadingIndicatorService,
    private categoriesService: CategoriesRepositoryService,
    private bottomSheetService: BottomSheetService,
    private usersRepo: UsersService,
    private authService: AuthService,
  ) { }

  ngOnInit(): void {
    const user = this.authService.getUserSync();
    forkJoin([
      this.usersRepo.fetchOne(user.email, { useCache: true }),
      this.categoriesService
        .fetchAll({ useCache: true })
    ])
      .subscribe(([userFull, categories]) => {
        this.categories = categories;
        this.selectedCategoryId = userFull.categoryId;
      });

    this.bottomSheetService.registerInstance(
      NameInstance.ChangeCategoryComponent,
      ChangeCategoryComponent
    );
  }

  public selectItem(data: ICategory): void {
    this.bottomSheetService.openChangeCategoryBottomSheet<ICategory>(data);
  }
}
