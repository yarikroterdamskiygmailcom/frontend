import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  selector: 'app-action-footer',
  templateUrl: './action-footer.component.html',
  styleUrls: ['./action-footer.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ActionFooterComponent {

}
