import { Component } from '@angular/core';
import { BreakpointObserver } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import { LoadingIndicatorService } from 'src/app/core/services/loading-indicator.service';

const MAX_WIDTH = '(max-width: 700px)';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss'],
})
export class NavigationComponent {
  public isLoading$: Observable<boolean>;
  public isHandset$: Observable<boolean> = this.breakpointObserver.observe(MAX_WIDTH)
    .pipe(
      map(({matches}) => matches),
      shareReplay()
    );

  constructor(
    private breakpointObserver: BreakpointObserver,
    private loadingIndicatorService: LoadingIndicatorService
  ) {
    this.isLoading$ = this.loadingIndicatorService.isLoading$;
  }

}
