import { Component, OnInit, ChangeDetectionStrategy, Output, EventEmitter, Input, OnDestroy } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { AuthService } from 'src/app/authentication/services/auth.service';
import { Observable, of, ReplaySubject, forkJoin } from 'rxjs';
import { BottomSheetService, NameInstance } from '../../../services/bottom.sheet.service';
import { UserMobileMenuComponent } from '../../user-mobile-menu/user-mobile-menu.component';
import { UsersService } from 'src/app/features/repositories/users.service';
import { switchMap, catchError, filter, tap, takeUntil, map } from 'rxjs/operators';
import { IUser } from 'src/app/core/interfaces/resources/IUser';
import { CategoriesRepositoryService } from 'src/app/features/repositories/categories-repository.service';

const instances = {
  [NameInstance.UserMobileMenuComponent]: UserMobileMenuComponent,
};
@Component({
  selector: 'app-top-nav',
  templateUrl: './top-nav.component.html',
  styleUrls: ['./top-nav.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TopNavComponent implements OnInit, OnDestroy {
  @Input() public showToggleButton: boolean;

  @Output() private sideNavToggled = new EventEmitter<void>();

  public badgeCount = 6;
  public user$: Observable<firebase.User>;
  public profile$: Observable<IUser>;
  public categoryName: string;

  private componentDestroyed$ = new ReplaySubject<boolean>(1);

  constructor(
    private authService: AuthService,
    private readonly router: Router,
    private bottomSheet: BottomSheetService,
    private usersRepo: UsersService,
    private categoriesRepo: CategoriesRepositoryService,
  ) { }

  ngOnInit() {
    this.bottomSheet.registerInstance(
      NameInstance.UserMobileMenuComponent,
      instances[NameInstance.UserMobileMenuComponent]
    );
    this.user$ = this.authService.getUser();
    this.profile$ = this.authService.getUser()
      .pipe(
        takeUntil(this.componentDestroyed$),
        switchMap(user => {
          return forkJoin([
            this.usersRepo.fetchOne(user.email, {}),
            this.categoriesRepo.fetchAll({ useCache: true })
          ])
            .pipe(
              switchMap(() => {
                return this.usersRepo.getStream()
                  .pipe(
                    map(profiles => profiles.find(u => u.id === user.email)),
                    tap(profile => {
                      if (profile) {
                        this.categoryName = this.categoriesRepo.getStreamValue()
                          .find(c => c.id === profile.categoryId)?.name;
                      }
                    })
                  );
              }),
              catchError(() => {
                this.router.navigate(['/profile']);
                this.blockUntilProfileExists();
                return of(null);
              })
            );
        })
      );
  }

  private blockUntilProfileExists() {
    this.router.events
      .pipe(
        filter((e): e is NavigationEnd => {
          return e instanceof NavigationEnd && e.url !== 'profile';
        }),
        tap(() => {
          const email = this.authService.getUserSync().email;
          const profile = this.usersRepo.getStreamValue()
            .find(p => p.id === email);
          if (!profile) {
            this.router.navigate(['/profile']);
          }

        })
      ).subscribe();
  }

  public toggleSidebar() {
    this.sideNavToggled.emit();
  }

  public logout(): void {
    this.authService.logout()
      .subscribe(() => {
        this.router.navigate(['/login']);
      });
  }

  public navigate(path: string): void {
    this.router.navigate([path]);
  }

  public openMobileMenu(): void {
    this.bottomSheet.openUserMenuBottomSheet();
  }

  ngOnDestroy() {
    this.componentDestroyed$.next(true);
    this.componentDestroyed$.complete();
  }
}
