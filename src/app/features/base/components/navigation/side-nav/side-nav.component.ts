import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { ISidenavItem } from 'src/app/core/interfaces/ISidenavItem';
import { Router } from "@angular/router";

@Component({
  selector: 'app-side-nav',
  templateUrl: './side-nav.component.html',
  styleUrls: ['./side-nav.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SideNavComponent {
  showMenu = false;
  public routes: ISidenavItem[] = [
    {
      icon: 'home',
      label: 'Home',
      path: 'home'
    },
    {
      icon: 'verified_user',
      label: 'Sanctuary',
      path: 'sanctuary'
    },
    {
      icon: 'group_work',
      label: 'Community',
      path: 'community',
      children: [
        {
          label: 'Blogs',
          path: 'blogs',
        },
        {
          label: 'Groups',
          path: 'groups',
        },
        {
          label: 'Crowd',
          path: 'crowd',
        },
        {
          label: 'Tweets',
          path: 'tweets',
        },
        {
          label: 'News',
          path: 'news',
        },
        {
          label: 'Vote',
          path: 'vote',
        },
      ]
    },
    {
      icon: 'portrait',
      label: 'Experts',
      path: 'experts'
    },
    {
      icon: 'navigation',
      label: 'Navigator',
      path: 'navigator'
    },
    {
      icon: 'fullscreen_exit',
      label: 'Categories',
      path: 'categories'
    },
  ];
  constructor(private router: Router) {

  }

  public isRouteActive(url: string): boolean {
    return this.router.url.includes(url);
  }

}
