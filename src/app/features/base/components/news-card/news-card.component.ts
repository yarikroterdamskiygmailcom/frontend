import { Component, OnInit, ChangeDetectionStrategy, Input } from '@angular/core';
import { INews } from 'src/app/core/interfaces/resources/INews';

@Component({
  selector: 'app-news-card',
  templateUrl: './news-card.component.html',
  styleUrls: ['./news-card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class NewsCardComponent implements OnInit {
  @Input() public newsItem: INews;
  constructor() { }

  ngOnInit(): void {   }

  public openLink(link: string) {
    window.open(link, '_blank');
  }
}
