import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  Output
} from '@angular/core';
import { IOption } from 'src/app/core/interfaces/IOption';

@Component({
  selector: 'app-tab-button',
  templateUrl: './tab-button.component.html',
  styleUrls: ['./tab-button.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TabButtonComponent {
  @Input() public selectedValue: string | number;
  @Input() public items: IOption[];
  @Output() public tabChange: EventEmitter<string> = new EventEmitter<string>();

  public changeValue({ value }) {
    this.tabChange.emit(value);
  }
}
