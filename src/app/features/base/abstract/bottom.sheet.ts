import { MatBottomSheet } from '@angular/material/bottom-sheet';

const MODAL_BACKGROUND = 'bottom-sheet';
const MODAL_BACKGROUND_NO_PADDING = 'bottom-sheet-not-padding';

export abstract class BottomSheet {
    private contextInstances: any = {};

    protected constructor(
        private _bottomSheet: MatBottomSheet
    ) {
    }

    public registerInstance(name: string, component: any): void {
        if (this.contextInstances[name]){
            return;
        }
        this.contextInstances[name] = component;
    }

    public setContextInstances(object: object) {
        this.contextInstances = {
            ...this.contextInstances,
            ...object
        };
    }

    public openBottomSheet<T>(
        name: string,
        isNoPadding?: boolean,
        data?: T
    ) {
        this._bottomSheet.open(
            this.contextInstances[name],
            {
                backdropClass: isNoPadding ? MODAL_BACKGROUND_NO_PADDING : MODAL_BACKGROUND,
                data
            }
        );
    }
}

