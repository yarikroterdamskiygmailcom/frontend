import { Directive, Input, ElementRef, Renderer2, OnInit } from '@angular/core';
import { getRandomInteger } from '../../../core/helpers/general.helper';

export const COLOR_CLASSES_PALETTE = [
  'background-red',
  'background-green',
  'background-blue',
  'background-grey',
  'background-orange',
];

const COLOR_CLASSES_MAP = {};


@Directive({
  selector: '[colorize]'
})
export class ColorizeDirective implements OnInit {
  protected _id: any;

  @Input('colorize') set id(value: any) {
    this._id = value;
    this.setClass();
  }

  constructor(
    protected ref: ElementRef,
    protected renderer: Renderer2
  ) {
  }

  ngOnInit(): void {
    this.setClass();
  }

  public setClass() {
    const colorClass = this.getColorClass(this._id);

    this.saveColor(this._id, colorClass);
    this.renderer.addClass(this.ref.nativeElement, colorClass);
  }

  private saveColor(id: any, colorClass: string) {
    if (id && !COLOR_CLASSES_MAP[id]) {
      COLOR_CLASSES_MAP[id] = colorClass;
    }
  }

  private getColorClass(id?: any) {
    if (id && COLOR_CLASSES_MAP[id]) {
      return COLOR_CLASSES_MAP[id];
    }

    return COLOR_CLASSES_PALETTE[
      getRandomInteger(
        0,
        COLOR_CLASSES_PALETTE.length - 1)
    ];
  }
}
