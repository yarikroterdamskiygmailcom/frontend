import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { BaseRoutingModule, BASE_COMPONENTS } from './base-routing.module';
import { ColorizeDirective } from './directives/colorize.directive';
import { ActionFooterComponent } from './components/action-footer/action-footer.component';
import { AttachmentButtonComponent } from './components/attachment-button/attachment-button.component';
import { AvatarComponent } from './components/avatar/avatar.component';
import { TabButtonComponent } from './components/tab-button/tab-button.component';
import { UserMobileMenuComponent } from './components/user-mobile-menu/user-mobile-menu.component';

const DECLERATIONS = [
  ActionFooterComponent,
  AttachmentButtonComponent,
  AvatarComponent,
  TabButtonComponent,
  UserMobileMenuComponent,
];

@NgModule({
  declarations: [
    ...DECLERATIONS,
    ...BASE_COMPONENTS,
    ColorizeDirective,
  ],
  imports: [
    SharedModule,
    BaseRoutingModule,
  ],
  exports: [
    ...DECLERATIONS,
    ...BASE_COMPONENTS,
    ColorizeDirective,
    SharedModule,
  ]
})
export class BaseModule { }
