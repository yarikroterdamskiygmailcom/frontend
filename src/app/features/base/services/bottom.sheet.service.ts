import { Injectable } from '@angular/core';
import { MatBottomSheet } from '@angular/material/bottom-sheet';
import { BottomSheet } from '../abstract/bottom.sheet';

export const NameInstance = {
  GroupBottomSheetCreateGroupComponent: 'GroupBottomSheetCreateGroupComponent',
  GroupBottomSheetManageComponent: 'GroupBottomSheetManageComponent',
  GroupBottomSheetMessageComponent: 'GroupBottomSheetMessageComponent',
  UserMobileMenuComponent: 'UserMobileMenuComponent',
  ChangeCategoryComponent: 'ChangeCategoryComponent',
};

@Injectable({
  providedIn: 'root'
})
export class BottomSheetService extends BottomSheet {
  constructor(
    _bottomSheet: MatBottomSheet
  ) {
    super(_bottomSheet);
    this.setContextInstances({
      [NameInstance.GroupBottomSheetCreateGroupComponent]: null,
      [NameInstance.GroupBottomSheetManageComponent]: null,
      [NameInstance.GroupBottomSheetMessageComponent]: null,
      [NameInstance.UserMobileMenuComponent]: null,
      [NameInstance.ChangeCategoryComponent]: null,
    });
  }

  public openBottomSheetManage(): void {
    this.openBottomSheet(
      NameInstance.GroupBottomSheetManageComponent,
      true
    );
  }

  public openBottomSheetMessage(): void {
    this.openBottomSheet(
      NameInstance.GroupBottomSheetMessageComponent
    );
  }

  public openBottomSheetCreateGroup(): void {
    this.openBottomSheet(
      NameInstance.GroupBottomSheetCreateGroupComponent,
      true
    );
  }

  public openUserMenuBottomSheet(): void {
    this.openBottomSheet(
      NameInstance.UserMobileMenuComponent,
      true
    );
  }

  public openChangeCategoryBottomSheet<T>(data: T): void {
    this.openBottomSheet<T>(
      NameInstance.ChangeCategoryComponent,
      true,
      data
    );
  }
}
