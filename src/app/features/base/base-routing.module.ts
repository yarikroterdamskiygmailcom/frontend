import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NavigationComponent } from './components/navigation/navigation.component';
import { SideNavComponent } from './components/navigation/side-nav/side-nav.component';
import { TopNavComponent } from './components/navigation/top-nav/top-nav.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { DashboardRightSidebarComponent } from './components/dashboard/dashboard-right-sidebar/dashboard-right-sidebar.component';
import { BaseComponent } from './components/base/base.component';
import { DashboardPostsComponent } from './components/dashboard/dashboard-posts/dashboard-posts.component';
import { DashboardTweetsComponent } from './components/dashboard/dashboard-tweets/dashboard-tweets.component';
import { DashboardNewsComponent } from './components/dashboard/dashboard-news/dashboard-news.component';
import { PostCardComponent } from './components/post-card/post-card.component';
import { NewsCardComponent } from './components/news-card/news-card.component';
import { BaseGuard } from './guards/base.guard';

// Community module (we need it to show news in the dashboard)
import { NewsComponent } from '../community/components/news/news.component';
import { BlogsComponent } from '../community/components/blogs/blogs.component';
import { ProfileComponent } from './components/profile/profile.component';
import { CategoriesComponent } from './components/categories/categories.component';
import { ChangeCategoryComponent } from './components/change-category/change-category.component';
import { LandingComponent } from './components/landing/landing.component';

export const BASE_COMPONENTS = [
  NavigationComponent,
  TopNavComponent,
  SideNavComponent,
  DashboardComponent,
  DashboardRightSidebarComponent,
  BaseComponent,
  DashboardPostsComponent,
  DashboardTweetsComponent,
  DashboardNewsComponent,
  PostCardComponent,
  NewsCardComponent,
  NewsComponent,
  BlogsComponent,
  ProfileComponent,
  CategoriesComponent,
  ChangeCategoryComponent,
  LandingComponent,
];

const routes: Routes = [
  {
    path: '',
    component: BaseComponent,
    canActivate: [BaseGuard],
    children: [
      {
        path: '',
        redirectTo: 'home',
        pathMatch: 'full'
      },
      {
        path: 'home',
        component: DashboardComponent,
      },
      {
        path: 'profile',
        component: ProfileComponent,
      },
      {
        path: 'categories',
        component: CategoriesComponent,
      },
      {
        path: 'landing',
        component: LandingComponent,
      },
      {
        path: 'community',
        loadChildren: () => import('../community/community.module')
            .then(c => c.CommunityModule),
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BaseRoutingModule { }
