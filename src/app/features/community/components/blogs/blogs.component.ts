import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { Observable, of } from 'rxjs';
import { IPost } from '../../../../core/interfaces/resources/IPost';
import { map } from 'rxjs/operators';
import { LoadingIndicatorService } from '../../../../core/services/loading-indicator.service';
import { IBlog } from '../../../../core/interfaces/resources/IBlog';
import { BLOGS_MOCK } from 'src/app/core/data/mock/blogs.mock';

@Component({
  selector: 'app-blogs',
  templateUrl: './blogs.component.html',
  styleUrls: ['./blogs.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class BlogsComponent implements OnInit {
  public search = false;
  public posts$: Observable<IPost[]>;
  public blogs$: Observable<IBlog[]>;

  constructor(
    public loadingService: LoadingIndicatorService
  ) { }

  ngOnInit(): void {
    this.posts$ = of(BLOGS_MOCK)
      .pipe(
        map(this.filterLastThreeElement)
      );
  }

  private filterLastThreeElement = items => {
    return Array.isArray(items) ? items.slice(0, 3) : [];
  }
}
