import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  templateUrl: './group-bottom-sheet-message.component.html',
  styleUrls: ['./group-bottom-sheet-message.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class GroupBottomSheetMessageComponent {
  public newMessage = '';
}
