import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { BLOGS_MOCK } from 'src/app/core/data/mock/blogs.mock';
import { NameInstance, BottomSheetService } from 'src/app/features/base/services/bottom.sheet.service';
import { GroupBottomSheetCreateGroupComponent } from './group-bottom-sheet-create-group/group-bottom-sheet-create-group.component';
import { GroupBottomSheetManageComponent } from './group-bottom-sheet-manage/group-bottom-sheet-manage.component';
import { GroupBottomSheetMessageComponent } from './group-bottom-sheet-message/group-bottom-sheet-message.component';
import { IPost } from 'src/app/core/interfaces/resources/IPost';
import { LoadingIndicatorService } from 'src/app/core/services/loading-indicator.service';

const instances = {
  [NameInstance.GroupBottomSheetCreateGroupComponent]: GroupBottomSheetCreateGroupComponent,
  [NameInstance.GroupBottomSheetManageComponent]: GroupBottomSheetManageComponent,
  [NameInstance.GroupBottomSheetMessageComponent]: GroupBottomSheetMessageComponent,
};

const instancesName = Object.keys(instances);

@Component({
  selector: 'app-groups',
  templateUrl: './groups.component.html',
  styleUrls: ['./groups.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class GroupsComponent implements OnInit {
  public search = false;
  public posts$: Observable<IPost[]>;

  constructor(
    public loadingService: LoadingIndicatorService,
    public provider: BottomSheetService,
  ) { }

  public openBottomSheetMessage(): void {
    this.provider.openBottomSheetMessage();
  }

  public openBottomSheetManage(): void {
    this.provider.openBottomSheetManage();
  }

  ngOnInit(): void {
    this.setContextBottomSheet();
    this.posts$ = of(BLOGS_MOCK)
      .pipe(
        map(this.filterLastThreeElement)
      );
  }

  private setContextBottomSheet() {
    instancesName.forEach((item: string) => {
      if (!instances[item]) {
        return;
      }
      this.provider.registerInstance(item, instances[item]);
    });
  }

  private filterLastThreeElement = (data) => Array.isArray(data) ? data.slice(0, 7) : [];
}
