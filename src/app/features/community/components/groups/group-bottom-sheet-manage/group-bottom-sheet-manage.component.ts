import { ChangeDetectionStrategy, Component } from '@angular/core';
import { FormControl } from '@angular/forms';
import { BottomSheetService } from 'src/app/features/base/services/bottom.sheet.service';
import { enumToArray } from 'src/app/core/helpers/general.helper';
import { IOption } from 'src/app/core/interfaces/IOption';

enum GROUP_VIEWS {
  GROUP_MY = 'My Groups',
  MANAGE_BY_ME = 'Managed by me',
  ALL_GROUPS = 'All groups'
}

export interface IItemsList {// TODO
  name: string;
  full: boolean;
}

@Component({
  selector: 'app-group-bottom-sheet',
  templateUrl: './group-bottom-sheet-manage.component.html',
  styleUrls: ['./group-bottom-sheet-manage.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class GroupBottomSheetManageComponent {
  public searchControl: FormControl = new FormControl('');
  public groupItems: IOption[] = enumToArray(GROUP_VIEWS);
  public selectedView: string = GROUP_VIEWS.GROUP_MY;

  public _items: IItemsList[] = [ // TODO
    { name: 'item 1', full: false },
    { name: 'item 2', full: true },
    { name: 'item 3', full: true },
    { name: 'item 4', full: false },
    { name: 'item 5', full: false },
    { name: 'item 6', full: true },
  ];

  get items(): IItemsList[] {
    return this._items.filter(
      (_: IItemsList): boolean => _.name.includes(this.searchControl.value)
    );
  }

  constructor(
    private provider: BottomSheetService
  ) {
  }

  public onViewChange(event: any): void {
    // TODO
  }

  public openBottomSheet(): void {
    this.provider.openBottomSheetCreateGroup();
  }
}
