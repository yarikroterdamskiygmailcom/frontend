import { ChangeDetectionStrategy, Component } from '@angular/core';
import { BottomSheetService } from 'src/app/features/base/services/bottom.sheet.service';
import { enumToArray } from 'src/app/core/helpers/general.helper';
import { IOption } from 'src/app/core/interfaces/IOption';

enum ACCESS_VIEWS {
  PUBLIC = 'Public',
  PRIVATE = 'Private',
}

@Component({
  selector: 'app-group-bottom-sheet-create-group',
  templateUrl: './group-bottom-sheet-create-group.component.html',
  styleUrls: ['./group-bottom-sheet-create-group.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class GroupBottomSheetCreateGroupComponent {
  public accessItems: IOption[] = enumToArray(ACCESS_VIEWS);
  public selectedView: string = ACCESS_VIEWS.PUBLIC;

  public onViewChange(event: any): void {
    // TODO
  }

  constructor(
    private provider: BottomSheetService
  ) { }

  public openBottomSheetMessage(): void {
    this.provider.openBottomSheetManage();
  }
}
