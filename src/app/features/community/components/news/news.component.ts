import { Component, OnInit, ChangeDetectionStrategy, Input } from '@angular/core';
import { FeedlyRepositoryService } from '../../../repositories/feedly-repository.service';
import { IFeedlyCollection } from '../../../../core/interfaces/resources/IFeedly';
import { forkJoin, BehaviorSubject, Observable, of } from 'rxjs';
import { ICategory } from '../../../../core/interfaces/resources/ICategory';
import { tap, map, distinctUntilChanged, debounceTime, switchMap } from 'rxjs/operators';
import { GeneralCancer } from '../../../../core/data/constants';
import { INews } from 'src/app/core/interfaces/resources/INews';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { extractContent } from 'src/app/core/helpers/general.helper';
import { LoadingIndicatorService } from 'src/app/core/services/loading-indicator.service';
import { FormControl } from '@angular/forms';
import { NewsSourcesRepositoryService } from 'src/app/features/repositories/news-sources-repository.service';
@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class NewsComponent implements OnInit {
  @Input() dashboardView: boolean;

  private userCategoryFeed: IFeedlyCollection = {} as IFeedlyCollection;
  private generalCancerFeed: IFeedlyCollection = {} as IFeedlyCollection;
  private userCategoryFeedContinuation = '';
  private generalCancerFeedContinuation = '';
  // TODO: Remove this and use real category
  private userCategory: ICategory = { id: '1', name: 'Myeloma' };

  private dataSourceSubject = new BehaviorSubject<INews[]>([]);
  private _feedlyDataSource: INews[] = [];
  private _newSourceDataSource: INews[] = [];

  public feedlyDataSource$: Observable<INews[]> = this.dataSourceSubject.asObservable();
  public sortByCtrl = new FormControl();
  public searchControl = new FormControl('');


  constructor(
    private feedlyRepositoryService: FeedlyRepositoryService,
    private breakpointObserver: BreakpointObserver,
    public loadingService: LoadingIndicatorService,
    private newsSourcesRepo: NewsSourcesRepositoryService,
  ) { }

  public isHandset$: Observable<boolean> = this.breakpointObserver
    .observe(Breakpoints.Handset)
    .pipe(map(result => result.matches));

  ngOnInit(): void {
    this.fetchFromNewsSources();
    this.fetchFromFeedlySources();

    this.searchControl.valueChanges
      .pipe(
        distinctUntilChanged(),
        debounceTime(500),
        tap((searchText: string) => this.filter(searchText.toLowerCase()))
      ).subscribe();
  }

  private fetchFromNewsSources() {
    this.newsSourcesRepo.fetchAll({})
      .pipe(
        switchMap(sources => {
          // TODO: Filter by user category
          return forkJoin([
            ...sources.map(s => this.newsSourcesRepo.fetchNewsByUrl(s.url))
          ]);
        }),
        map(sources => {
          return sources.map(s => s.items.map(i => {
            return {
              title: i.title,
              text: extractContent(i.description).replace(/(\r\n|\n|\r)/gm, ' ').trim(),
              date: new Date(i.pubDate).getTime(),
              imageUrl: i.thumbnail,
              origin: i.link
            } as INews;
          }));
        }),
        map(items => [].concat.apply([], items)),
        tap(items => {
          this._newSourceDataSource = items;
          this.appendToViewList(items, true);
        })
      )
      .subscribe();
  }

  private fetchFromFeedlySources() {
    this.feedlyRepositoryService.fetchCollections(this.userCategory)
      .pipe(
        tap(collections => {
          this.userCategoryFeed = collections.find(c => c.label === this.userCategory.name);
          this.generalCancerFeed = collections.find(c => c.label === GeneralCancer);
          this.fetchStreams();
        })
      ).subscribe();
  }

  private filter(searchText: string) {
    if (searchText.length) {
      const filteredSource = [
        ...this._feedlyDataSource,
        ...this._newSourceDataSource
      ].filter(n => n.text.toLowerCase().includes(searchText)
        || n.title.toLowerCase().includes(searchText));
      this.appendToViewList(filteredSource);
    } else {
      this.appendToViewList([
        ...this._newSourceDataSource,
        ...this._feedlyDataSource
      ]);
    }
  }

  private fetchStreams() {
    return forkJoin([
      this.fetchGeneralCancerStream(),
      this.fetchUserCategoryStream(),
    ]).pipe(
      map(([stream1, stream2]) => {
        return [...stream2.items, ...stream1.items].map(i => {
          // TODO: This is a bug by feedly sendind invalid urls with additional text
          // https://trello.com/c/RCCGdzgD/426-feedly-sometimes-returns-bad-url
          const text: string = i?.summary?.content || '';
          let origin = i?.canonicalUrl || i?.originId;
          if (!origin.startsWith('http') && origin?.length) {
            const cleanUrl = origin.match(/\bhttps?:\/\/\S+/gi);
            origin = cleanUrl?.length ? cleanUrl[0] : null;
          }
          return {
            title: i.title,
            text: extractContent(text),
            date: i.published,
            imageUrl: i?.visual?.url,
            origin,
          } as INews;
        });
      })
    ).subscribe((news: INews[]) => {
      this._feedlyDataSource = news;
      this.appendToViewList(news);
    });
  }

  private fetchGeneralCancerStream() {
    return this.feedlyRepositoryService.fetchStreams(
      this.generalCancerFeed.id,
      10,
      this.generalCancerFeedContinuation
    ).pipe(
      tap(stream => {
        this.generalCancerFeedContinuation = stream.continuation;
      })
    );
  }

  private fetchUserCategoryStream() {
    return this.feedlyRepositoryService.fetchStreams(
      this.userCategoryFeed.id,
      10,
      this.userCategoryFeedContinuation
    ).pipe(
      tap(stream => {
        this.userCategoryFeedContinuation = stream.continuation;
      })
    );
  }

  private appendToViewList(items: INews[], showFirst = false): void {
    if (showFirst) {
      return this.dataSourceSubject.next([
        ...items,
        ...this.dataSourceSubject.value,
      ]);
    }
    return this.dataSourceSubject.next([
      ...this.dataSourceSubject.value,
      ...items,
    ]);
  }
}
