import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NewsComponent } from './components/news/news.component';
import { BlogsComponent } from './components/blogs/blogs.component';
import { GroupsComponent } from './components/groups/groups.component';

export const COMMUNITY_COMPONENTS = [
  GroupsComponent,
];

const routes: Routes = [
  { path: '', redirectTo: 'news' },
  { path: 'blogs', component: BlogsComponent },
  { path: 'news', component: NewsComponent },
  { path: 'groups', component: GroupsComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CommunityRoutingModule { }
