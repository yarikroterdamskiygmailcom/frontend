import { NgModule } from '@angular/core';
import { CommunityRoutingModule, COMMUNITY_COMPONENTS } from './community-routing.module';
import { BaseModule } from '../base/base.module';
import { GroupBottomSheetCreateGroupComponent } from './components/groups/group-bottom-sheet-create-group/group-bottom-sheet-create-group.component';
import { GroupBottomSheetManageComponent } from './components//groups/group-bottom-sheet-manage/group-bottom-sheet-manage.component';
import { GroupBottomSheetMessageComponent } from './components/groups/group-bottom-sheet-message/group-bottom-sheet-message.component';
import { GroupCardComponent } from './components/groups/group-card/group-card.component';

@NgModule({
  imports: [
    BaseModule,
    CommunityRoutingModule,
  ],
  declarations: [
    ...COMMUNITY_COMPONENTS,
    GroupBottomSheetCreateGroupComponent,
    GroupBottomSheetManageComponent,
    GroupBottomSheetMessageComponent,
    GroupCardComponent,
  ]
})
export class CommunityModule {
}
