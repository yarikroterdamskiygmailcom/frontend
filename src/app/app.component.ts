import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { LoadingIndicatorService } from './core/services/loading-indicator.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  public isLoading$: Observable<boolean>;

  constructor(
    private loadingService: LoadingIndicatorService
  ) {
  }

  ngOnInit() {
    this.isLoading$ = this.loadingService.isLoading$;
  }
}
