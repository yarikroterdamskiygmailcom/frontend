import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LayoutModule } from '@angular/cdk/layout';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { MaterialModule } from '../material/material.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { IconButtonComponent } from './components/icon-button/icon-button.component';
import { MchIconComponent } from './components/mch-icon/mch-icon.component';
import { NotificationCardComponent } from './components/notification-card/notification-card.component';
import { StarRatingComponent } from './components/star-rating/star-rating.component';
import { EllipsisPipe } from './pipes/ellipsis.pipe';
import { LoadingIndicatorComponent } from './components/loading-indicator/loading-indicator.component';

const COMPONENTS = [
  MchIconComponent,
  IconButtonComponent,
  StarRatingComponent,
  NotificationCardComponent,
  EllipsisPipe,
  LoadingIndicatorComponent,
];

const imports = [
  CommonModule,
  FormsModule,
  ReactiveFormsModule,
  LayoutModule,
  RouterModule,
  HttpClientModule,
  MaterialModule,
];

@NgModule({
  declarations: [
    ...COMPONENTS,
  ],
  imports: [
    ...imports
  ],
  exports: [
    ...COMPONENTS,
    ...imports
  ]
})
export class SharedModule { }
