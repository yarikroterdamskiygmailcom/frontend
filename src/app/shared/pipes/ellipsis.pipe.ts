import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'ellipsis'
})
export class EllipsisPipe implements PipeTransform {
  transform(val: string | Array<any>, args: number) {
    if (val instanceof Array) {
      val = val.join(', ');
    }
    if (args === undefined) {
      return val;
    }
    return (val && val.length > args) ? val.substring(0, args) + '...' : val;
  }
}
