import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

@Component({
  selector: 'app-star-rating',
  templateUrl: './star-rating.component.html',
  styleUrls: ['./star-rating.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class StarRatingComponent {
  @Input()
  public rating: number;
  @Input()
  public max = 5;

  constructor() { }

  public get fullStarCount(): number[] {
    const fullStarCount = Number(
      this.rating.toString().split('.')[0]
    );
    return Array(fullStarCount).fill(1);
  }

  public get hasHalfStar(): boolean {
    return !!this.rating.toString().split('.')[1];
  }

  public get emptyStarCount(): number[] {
    const fullStarCount = Number(
      this.rating.toString().split('.')[0]
    );
    if (fullStarCount === this.max) {
      return [];
    }
    return Array(this.max - 1 - Number(
      this.rating.toString().split('.')[0]
    )).fill(1);
  }

}
