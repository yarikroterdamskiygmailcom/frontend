import { Component, OnInit, ChangeDetectionStrategy, Input, Output } from '@angular/core';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-icon-button',
  templateUrl: './icon-button.component.html',
  styleUrls: ['./icon-button.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class IconButtonComponent {
  @Input()
  public icon: string;

  @Output()
  public clicked = new Subject<null>();

  constructor() { }

  public emitOnClick(): void {
    this.clicked.next(null);
  }

}
