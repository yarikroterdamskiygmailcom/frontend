import { Component, OnInit, ChangeDetectionStrategy, Input } from '@angular/core';

@Component({
  selector: 'app-mch-icon',
  templateUrl: './mch-icon.component.html',
  styleUrls: ['./mch-icon.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MchIconComponent implements OnInit {
  // TODO: add no-icon as default one
  @Input() name: string;
  constructor() { }

  ngOnInit(): void {
  }

}
